package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002_CreateLeadPage extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLeadPage";
		testcaseDec = "Login into leaftaps and Create Lead";
		author = "DB";
		category = "smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String uName, String pwd,String ComName,String Fname,String Lname) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickcrmsfa()
		.clickleads()
		.clickCreateLead()
		.enterComName(ComName)
		.enterFristName(Fname)
		.enterLastname(Lname)
		.clickCreateLead()
		.verifyCreateLead(Fname);
			
	}
}






