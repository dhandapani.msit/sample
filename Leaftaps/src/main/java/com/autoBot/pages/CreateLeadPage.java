package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations{ 

	public CreateLeadPage enterComName(String ComName) {
		WebElement elecomname=locateElement("ID","createLeadForm_companyName");
		clearAndType(elecomname,ComName);
		return this;
	}
	
	public CreateLeadPage enterFristName(String Fname) {
		WebElement elecomname=locateElement("ID","createLeadForm_firstName");
		clearAndType(elecomname,Fname);
		return this;
	}
	public CreateLeadPage enterLastname(String Lname) {
		WebElement elecomname=locateElement("ID","createLeadForm_lastName");
		clearAndType(elecomname,Lname);
		return this;
	}
	
	public ViewLeadsPage clickCreateLead() {
		WebElement elecomname=locateElement("name","submitButton");
		click(elecomname);
		return new ViewLeadsPage();
	}
}







